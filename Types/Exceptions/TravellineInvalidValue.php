<?php


namespace  Travelline\Types\Exceptions;

use Exception;

/**
 * Исключение содержащая объект-ошибку
 */
class TravellineInvalidValue extends Exception
{

}
