<?php


namespace  Travelline\Types\PropertiesTypes;

use Exception;
use Travelline\Types\Exceptions\TravellineInvalidValue;

/**
 * Временная зона средства размещения
 */
class TimeZoneInfo
{
    /**
     * Название временной зоны. соответсвует Microsoft Time Zones
     * @var string
     */
    public $name;

    /**
     * @param array $array
     * @return self
     * @throws TravellineInvalidValue
     */
    public static function createFromArray(array $array): self
    {
        $object = new static();
        try {
            $object->name = $array['name'];
        } catch (Exception $e) {
            throw new TravellineInvalidValue("name is empty");
        }
        return $object;
    }


}