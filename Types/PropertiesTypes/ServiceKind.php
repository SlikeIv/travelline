<?php

namespace  Travelline\Types\PropertiesTypes;


/**
 * Тип услуги
 */
class ServiceKind
{
    /**
     * Common - общая услуга
     * @const string
     */
    const COMMON = 'Common';

    /**
     * Meal - питиание
     * @const string
     */
    const MEAL = 'Meal';
}
