<?php

namespace  Travelline\Types\PropertyEventTypes;


/**
 *Тип события, произошедшего со средством размещения:
 */
class PropertyEventType
{
    /**
     * Added - средство размещения добавлено
     * @const string
     */
    const ADDED = 'Added';

    /**
     * Modified - изменены данные
     * @const string
     */
    const MODIFIED  = 'Modified';

}



