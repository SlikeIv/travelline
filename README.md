
## Библиотека для работы с TravelLine Partner - Content API (v1)
https://partner.qatl.ru/docs/booking-process/

Для работы нужен пакет:
+ Guzzle (composer require guzzlehttp/guzzle:^7.0)


Использование:

```php

use Travelline\ContentApi;
use Travelline\Types\Exceptions\TravellineBadResponse;

$apiKey = '<YOU_API_KEY>';
$contentApi = new ContentApi($apiKey);

// GET EVENTS
try {       
    $continue ="123456...";
    $timestamp = null;
    // or
    $continue = null;
    $timestamp = \DateTime::createFromFormat(\DateTime::ISO8601, '2019-06-20T10:41:04Z');
    
    $count = 50;
    $contentApi = $contentApi->getEvents($continue, $timestamp, $count);
    $events = $contentApi->getEvents();
    echo "<PRE>";
    var_dump($events);
    echo "</PRE>";
} catch (TravellineBadResponse $e) {
    print_r($e->errorsResponse);  // ErrorResponse object or null
    echo $e->httpStatus;          // integer
    print_r($e->response);        // array
}

// GET PROPERTIES
try {
    $since ="abcdef...";
    $count = 100;
    $properties = $contentApi->getProperties($since, $count);
    echo "<PRE>";
    var_dump($properties);
    echo "</PRE>";
} catch (TravellineBadResponse $e) {
    print_r($e->errorsResponse);  // ErrorResponse object or null
    echo $e->httpStatus;          // integer
    print_r($e->response);        // array
}


// GET PROPERTY BY ID
$propertyId = 1024;
try {
    $property = $contentApi->getPropertyById($propertyId);
    echo "<PRE>";
    var_dump($property);
    echo "</PRE>";
} catch (TravellineBadResponse $e) {
    print_r($e->errorsResponse);  // ErrorResponse object or null
    echo $e->httpStatus;          // integer
    print_r($e->response);        // array
}


// GET MEAL PLANS
$mealPlans = $contentApi->getMealPlans();
echo "<PRE>";
var_dump($mealPlans);
echo "</PRE>";



// GET ROOM TYPE CATEGORIES
$roomTypeCategories = $contentApi->getRoomTypeCategories();
echo "<PRE>";
var_dump($roomTypeCategories);
echo "</PRE>";


```
